let urls = [
    'https://ajax.test-danit.com/api/json/users',
    'https://ajax.test-danit.com/api/json/posts'
];
let requests = urls.map(url => fetch(url)
    .then((response) => {
        return response.json();
    }));


Promise.all(requests).then(([users, posts]) => {
    users.forEach(user => {
        const {name, email} = user;
        const postsUser = posts.filter(post => {
            if (post.userId === user.id) {
                return post;
            }


        })
        postsUser.forEach(post => {
            const {title, body, id} = post;
            // console.log(name,email,title,body,id);
            const card = new Card(id, name, email, title, body);
            card.render();
        })

    })
})

class Card {
    constructor(id, name, email, title, body) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
    }

    render() {
        document.body.insertAdjacentHTML("afterbegin", `<div id="${this.id}" style="background-color: #61a0ad"> <p><b>${this.name}</b></p> <p>${this.email}</p> <h3><i>${this.title}</i></h3> <p>${this.body}</p> <button id="${this.id}" class="button">Delete</button></div>`)
    }
}

    document.body.addEventListener("click", ev => {
       if (ev.target.classList.contains('button')){
           del(ev.target.id)
       }
    });

function del(btn) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${btn}`, {
        method: 'DELETE',
    })
        .then( () => {
        const cardToDelete = document.getElementById(`${btn}`);
        cardToDelete.remove();
    }
)}






